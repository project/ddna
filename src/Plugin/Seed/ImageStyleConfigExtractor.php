<?php

namespace Drupal\ddna\Plugin\Seed;

use Drupal\ddna\Annotation\Seed;
use Drupal\ddna\SeedPluginBase;

/**
 * Plugin implementation of the seed.
 *
 * @Seed(
 *   id = "image_style_config_extractor",
 *   label = @Translation("Image Style Config Extractor"),
 *   description = @Translation("Image Style Config Extractor.")
 * )
 */
class ImageStyleConfigExtractor extends SeedPluginBase {

  /**
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \JsonException
   */
  public function getElements(): string {
    return json_encode([
      'headers' => (object) $this->getTableHeader(),
      'rows' => $this->getTableRows(),
    ], JSON_THROW_ON_ERROR);
  }

  /**
   * @return string[]
   */
  protected function getTableHeader(): array {
    return [
      'style_name' => 'Style name',
      'machine_name' => 'Machine name',
      'effect_configuration' => 'Configuration',
      'effect' => 'Effect',
      'summary' => 'summary',
    ];
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTableRows(): array {
    $table_rows = [];
    $configs = \Drupal::service('ddna_config_matcher')
      ->matchAllConfigs($this->configuration['params']['regexp']);
    if (empty($configs)) {
      return [];
    }

    $image_styles = \Drupal::entityTypeManager()->getStorage('image_style')->loadMultiple();
    foreach ($configs as $element) {
      [, , $image_style_id] = explode('.', $element);
      if(empty($image_styles[$image_style_id])) {
        continue;
      }
      $image_style = $image_styles[$image_style_id];
      $image_style_effects = $image_style->getEffects();
      foreach($image_style_effects as $effect) {
        $effect_configuration = '';
        foreach($effect->configuration as $prop => $param) {
          $effect_configuration .= $prop . ': ' . $param . ' | ';
        }

        $table_rows[] = (object) [
          'style_name' => $image_style->getName(),
          'machine_name' => $image_style->id(),
          'effect' => $effect->getPluginId(),
          'effect_configuration' => $effect_configuration,
          'summary' => (string) $effect->getPluginDefinition()['description'],
        ];
      }
    }
    return $table_rows;
  }

}
