<?php

namespace Drupal\ddna\Plugin\Seed;

use Drupal\ddna\Annotation\Seed;
use Drupal\ddna\SeedPluginBase;

/**
 * Plugin implementation of the seed.
 *
 * @Seed(
 *   id = "workflow_config_extractor",
 *   label = @Translation("Workflow Config Extractor"),
 *   description = @Translation("Workflow Config Extractor.")
 * )
 */
class WorkflowConfigExtractor extends SeedPluginBase {

  /**
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \JsonException
   */
  public function getElements(): string {
    return json_encode([
      'headers' => (object) $this->getTableHeader(),
      'rows' => $this->getTableRows(),
    ], JSON_THROW_ON_ERROR);
  }

  /**
   * @return string[]
   */
  protected function getTableHeader(): array {
    return [
      'workflow_name' => 'Workflow name',
      'workflow_machine_name' => 'Workflow machine name',
      'workflow_type' => 'Workflow type',
      'entity_type' => 'Entity type',
      'state_name' => 'State name',
      'state_machine_name' => 'State machine name',
    ];
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTableRows(): array {
    $table_rows = [];
    $configs = \Drupal::service('ddna_config_matcher')
      ->matchAllConfigs($this->configuration['params']['regexp']);
    if (empty($configs)) {
      return [];
    }

    foreach ($configs as $element) {
      [, , $workflow_id] = explode('.', $element);
      $workflow = \Drupal::entityTypeManager()->getStorage('workflow')->load($workflow_id);
      $workflow_configuration = $workflow->getTypePlugin()->getConfiguration();
      foreach($workflow_configuration['states'] as $workflow_state_machine_name => $workflow_state) {
        foreach($workflow_configuration['entity_types'] as $workflow_entity) {
          $table_rows[] = (object) [
            'workflow_name' => $workflow->label(),
            'workflow_machine_name' => $workflow->id(),
            'workflow_type' => $workflow->getTypePlugin()->label(),
            'entity_type' => $workflow_entity,
            'state_name' => $workflow_state['label'],
            'state_machine_name' => $workflow_state_machine_name,
          ];
        }
      }
    }

    return $table_rows;
  }

}
