<?php

namespace Drupal\ddna\Plugin\Seed;

use Drupal\ddna\Annotation\Seed;
use Drupal\ddna\SeedPluginBase;

/**
 * Plugin implementation of the seed.
 *
 * @Seed(
 *   id = "workflow_transitions_config_extractor",
 *   label = @Translation("Workflow Transitions Config Extractor"),
 *   description = @Translation("Workflow Transitions Config Extractor.")
 * )
 */
class WorkflowTransitionsConfigExtractor extends SeedPluginBase {

  /**
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \JsonException
   */
  public function getElements(): string {
    return json_encode([
      'headers' => (object) $this->getTableHeader(),
      'rows' => $this->getTableRows(),
    ], JSON_THROW_ON_ERROR);
  }

  /**
   * @return string[]
   */
  protected function getTableHeader(): array {
    return [
      'workflow_name' => 'Workflow name',
      'workflow_transition_label' => 'Transition label',
      'workflow_transition_machine_name' => 'Transition machine name',
      'from_state' => 'From state',
      'to_state' => 'To state',
    ];
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTableRows(): array {
    $table_rows = [];
    $configs = \Drupal::service('ddna_config_matcher')
      ->matchAllConfigs($this->configuration['params']['regexp']);
    if (empty($configs)) {
      return [];
    }

    foreach ($configs as $element) {
      [, , $workflow_id] = explode('.', $element);
      $workflow = \Drupal::entityTypeManager()->getStorage('workflow')->load($workflow_id);
      $workflow_configuration = $workflow->getTypePlugin()->getConfiguration();
      foreach($workflow_configuration['transitions'] as $workflow_transition_machine_name => $workflow_transition) {
        foreach($workflow_transition['from'] as $from_state) {
          $table_rows[] = (object) [
            'workflow_name' => $workflow->label(),
            'workflow_transition_label' => $workflow_transition['label'],
            'workflow_transition_machine_name' => $workflow_transition_machine_name,
            'from_state' => $from_state,
            'to_state' => $workflow_transition['to'],
          ];
        }
      }
    }

    return $table_rows;
  }

}
