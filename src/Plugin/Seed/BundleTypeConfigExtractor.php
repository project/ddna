<?php

namespace Drupal\ddna\Plugin\Seed;

use Drupal\ddna\Annotation\Seed;
use Drupal\ddna\SeedPluginBase;

/**
 * Plugin implementation of the seed.
 *
 * @Seed(
 *   id = "bundle_type_config_extractor",
 *   label = @Translation("Bundle Type Config Extractor"),
 *   description = @Translation("Bundle Type Config Extractor.")
 * )
 */
class BundleTypeConfigExtractor extends SeedPluginBase {

  /**
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \JsonException
   */
  public function getElements(): string {
    return json_encode([
      'headers' => (object) $this->getTableHeader(),
      'rows' => $this->getTableRows(),
    ], JSON_THROW_ON_ERROR);
  }

  /**
   * @return string[]
   */
  protected function getTableHeader(): array {
    return [
      'name' => 'Name',
      'machine_name' => 'Machine name',
      'bundle_entity_name' => 'Bundle entity name',
      'description' => 'Description',
      'examples' => 'Example(s)',
      'moderated' => 'Moderated',
      'layout_builder' => 'Layout builder',
      'translatable' => 'Translatable',
      'migrated' => 'Migrated (Content will be populated via migration)',
      'scheduled' => 'Scheduled (Scheduled updates are enabled)',
      'searchable' => 'Searchable (Is indexed for site search)',
      'type' => 'Type',
      'url_alias_pattern' => 'Url alias pattern',
    ];
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTableRows(): array {
    $table_rows = [];
    $configs = \Drupal::service('ddna_config_matcher')
      ->matchAllConfigs($this->configuration['params']['regexp']);
    if (empty($configs)) {
      return [];
    }

    foreach ($configs as $element) {
      [$machine_name, ,] = explode('.', $element);
      /* @var \Drupal\Core\Entity\EntityTypeInterface $definition */
      $definition = \Drupal::entityTypeManager()->getDefinition($machine_name);
      $table_rows[] = (object) [
        'name' => (string) $definition->getLabel(),
        'machine_name' => $definition->id(),
        'bundle_entity_name' => $definition->getBundleEntityType(),
        'description' => '',
        'examples' => '-',
        'moderated' => '-',
        'layout_builder' => '-',
        'translatable' => $definition->isTranslatable() ? 'Yes' : 'No',
        'migrated' => '-',
        'scheduled' => '-',
        'searchable' => '-',
        'type' => $definition->getBundleLabel(),
        'url_alias_pattern' => '-',
      ];
    }

    return $table_rows;
  }

}
