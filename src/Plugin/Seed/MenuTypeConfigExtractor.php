<?php

namespace Drupal\ddna\Plugin\Seed;

use Drupal\ddna\Annotation\Seed;
use Drupal\ddna\SeedPluginBase;

/**
 * Plugin implementation of the seed.
 *
 * @Seed(
 *   id = "menu_type_config_extractor",
 *   label = @Translation("Menu Type Config Extractor"),
 *   description = @Translation("Menu Type Config Extractor.")
 * )
 */
class MenuTypeConfigExtractor extends SeedPluginBase {

  /**
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \JsonException
   */
  public function getElements(): string {
    return json_encode([
      'headers' => (object) $this->getTableHeader(),
      'rows' => $this->getTableRows(),
    ], JSON_THROW_ON_ERROR);
  }

  /**
   * @return string[]
   */
  protected function getTableHeader(): array {
    return [
      'title' => 'Title',
      'machine_name' => 'Machine name',
      'description' => 'Description',
    ];
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTableRows(): array {
    $table_rows = [];
    $configs = \Drupal::service('ddna_config_matcher')
      ->matchAllConfigs($this->configuration['params']['regexp']);
    if (empty($configs)) {
      return [];
    }

    $menus = \Drupal::entityTypeManager()->getStorage('menu')->loadMultiple();
    foreach ($configs as $element) {
      [, , $menu_id] = explode('.', $element);
      $table_rows[] = (object) [
        'title' => $menus[$menu_id]->label(),
        'machine_name' => $menus[$menu_id]->id(),
        'description' => $menus[$menu_id]->getDescription(),
      ];
    }

    return $table_rows;
  }

}
