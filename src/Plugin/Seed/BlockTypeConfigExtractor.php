<?php

namespace Drupal\ddna\Plugin\Seed;

use Drupal\ddna\Annotation\Seed;
use Drupal\ddna\SeedPluginBase;

/**
 * Plugin implementation of the seed.
 *
 * @Seed(
 *   id = "block_type_config_extractor",
 *   label = @Translation("Block Type Config Extractor"),
 *   description = @Translation("Block Type Config Extractor.")
 * )
 */
class BlockTypeConfigExtractor extends SeedPluginBase {

  /**
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \JsonException
   */
  public function getElements(): string {
    return json_encode([
      'headers' => (object) $this->getTableHeader(),
      'rows' => $this->getTableRows(),
    ], JSON_THROW_ON_ERROR);
  }

  /**
   * @return string[]
   */
  protected function getTableHeader(): array {
    return [
      'block_id' => 'Block ID',
      'theme_region' => 'Theme region',
      'theme' => 'Theme',
      'weight' => 'Weight',
    ];
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTableRows(): array {
    $table_rows = [];
    $configs = \Drupal::service('ddna_config_matcher')
      ->matchAllConfigs($this->configuration['params']['regexp']);
    if (empty($configs)) {
      return [];
    }

    $blocks = \Drupal::entityTypeManager()->getStorage('block')->loadMultiple();
    foreach ($configs as $element) {
      [, , $block_id] = explode('.', $element);
      $table_rows[] = (object) [
        'block_id' => $blocks[$block_id]->getPluginId(),
        'theme_region' => $blocks[$block_id]->getRegion(),
        'theme' => $blocks[$block_id]->getTheme(),
        'weight' => $blocks[$block_id]->getWeight(),
      ];
    }

    return $table_rows;
  }

}
