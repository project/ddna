<?php

namespace Drupal\ddna\Plugin\Seed;

use Drupal\ddna\Annotation\Seed;
use Drupal\ddna\SeedPluginBase;

/**
 * Plugin implementation of the seed.
 *
 * @Seed(
 *   id = "user_role_config_extractor",
 *   label = @Translation("User Roles Config Extractor"),
 *   description = @Translation("User Roles Config Extractor.")
 * )
 */
class UserRoleConfigExtractor extends SeedPluginBase {

  /**
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \JsonException
   */
  public function getElements(): string {
    return json_encode([
      'headers' => (object) $this->getTableHeader(),
      'rows' => $this->getTableRows(),
    ], JSON_THROW_ON_ERROR);
  }

  /**
   * @return string[]
   */
  protected function getTableHeader(): array {
    return [
      'role_name' => 'Role name',
      'machine_name' => 'Machine name',
    ];
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTableRows(): array {
    $table_rows = [];
    $configs = \Drupal::service('ddna_config_matcher')
      ->matchAllConfigs($this->configuration['params']['regexp']);
    if (empty($configs)) {
      return [];
    }

    $user_roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
    foreach ($configs as $element) {
      [, , $user_role_id] = explode('.', $element);
      $user_role = $user_roles[$user_role_id];
      $table_rows[] = (object) [
        'role_name' => $user_role->label(),
        'machine_name' => $user_role->id(),
      ];
    }

    return $table_rows;
  }

}
