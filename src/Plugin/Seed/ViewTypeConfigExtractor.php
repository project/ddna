<?php

namespace Drupal\ddna\Plugin\Seed;

use Drupal\ddna\Annotation\Seed;
use Drupal\ddna\SeedPluginBase;
use Drupal\views\Views;

/**
 * Plugin implementation of the seed.
 *
 * @Seed(
 *   id = "view_type_config_extractor",
 *   label = @Translation("Views Type Config Extractor"),
 *   description = @Translation("Views Type Config Extractor.")
 * )
 */
class ViewTypeConfigExtractor extends SeedPluginBase {

  /**
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \JsonException
   */
  public function getElements(): string {
    return json_encode([
      'headers' => (object) $this->getTableHeader(),
      'rows' => $this->getTableRows(),
    ], JSON_THROW_ON_ERROR);
  }

  /**
   * @return string[]
   */
  protected function getTableHeader(): array {
    return [
      'view_name' => 'View name',
      'base_table' => 'Base table',
      'machine_name' => 'Machine name',
      'display_machine_name' => 'Display machine name',
      'display_plugin' => 'Display plugin',
      'status' => 'Status',
      'description' => 'Description',
    ];
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTableRows(): array {
    $table_rows = [];
    $configs = \Drupal::service('ddna_config_matcher')
      ->matchAllConfigs($this->configuration['params']['regexp']);
    if (empty($configs)) {
      return [];
    }

    $views_displays_data = [];
    $views = \Drupal::entityTypeManager()->getStorage('view')->loadMultiple();
    $views_displays = Views::getViewsAsOptions();
    foreach ($views_displays as $config => $view) {
      [$view_machine_name, $display] = explode(':', $config);
      if (empty($views_displays_data[$view_machine_name])) {
        $views_displays_data[$view_machine_name] = [];
      }
      $views_displays_data[$view_machine_name][] = $display;
    }

    foreach ($configs as $element) {
      [, , $view_id] = explode('.', $element);
      foreach ($views_displays_data[$view_id] as $display) {
        $display = $views[$view_id]->getDisplay($display);
        $table_rows[] = (object) [
          'view_name' => urlencode($views[$view_id]->label()),
          'base_table' => $views[$view_id]->get('base_table'),
          'machine_name' => $views[$view_id]->id(),
          'display_machine_name' => $display['id'],
          'display_plugin' => $display['display_plugin'],
          'status' => $views[$view_id]->status() ? 'Enabled' : 'Disabled',
          'description' => $views[$view_id]->get('description'),
        ];
      }
    }

    return $table_rows;
  }

}
