<?php

namespace Drupal\ddna\Plugin\Seed;

use Drupal\ddna\Annotation\Seed;
use Drupal\ddna\SeedPluginBase;
use Drupal\field\FieldConfigInterface;

/**
 * Plugin implementation of the seed.
 *
 * @Seed(
 *   id = "field_type_config_extractor",
 *   label = @Translation("Field Type Config Extractor"),
 *   description = @Translation("Field Type Config Extractor.")
 * )
 */
class FieldTypeConfigExtractor extends SeedPluginBase {

  /**
   * @return string
   * @throws \JsonException
   */
  public function getElements(): string {
    return json_encode([
      'headers' => (object) $this->getTableHeader(),
      'rows' => $this->getTableRows(),
    ], JSON_THROW_ON_ERROR);
  }

  /**
   * @return string[]
   */
  protected function getTableHeader(): array {
    return [
      'bundle' => 'Bundle',
      'field_label' => 'Field label',
      'machine_name' => 'Machine name',
      'field_group' => 'Field group',
      'field_type' => 'Field type',
      'target_bundle' => 'Target bundle for reference fields',
      'required' => 'Required',
      'cardinality' => 'Cardinality, -1 means unlimited list',
      'form_widget' => 'Form widget',
      'help_text' => 'Help text',
    ];
  }

  /**
   * @return array
   */
  protected function getTableRows(): array {
    $table_rows = [];
    $configs = \Drupal::service('ddna_config_matcher')
      ->matchAllConfigs($this->configuration['params']['regexp']);
    if (empty($configs)) {
      return [];
    }

    foreach ($configs as $element) {
      [, , $entity_type_id, $bundle] = explode('.', $element);
      $base_fields_definition = \Drupal::service('entity_field.manager')
        ->getBaseFieldDefinitions($entity_type_id);
      $fields_definition = \Drupal::service('entity_field.manager')
        ->getFieldDefinitions($entity_type_id, $bundle);
      $field_storage_definition = \Drupal::service('entity_field.manager')
        ->getFieldStorageDefinitions($entity_type_id);
      $active_field_storage_definition = \Drupal::service('entity_field.manager')
        ->getActiveFieldStorageDefinitions($entity_type_id);
      $extra_fields = \Drupal::service('entity_field.manager')
        ->getExtraFields($entity_type_id, $bundle);

      foreach ($fields_definition as $definition) {
        /* @var FieldConfigInterface $definition */
        if (!$definition instanceof FieldConfigInterface) {
          continue;
        }

        $table_rows[] = (object) [
          'bundle' => $definition->getDependencies()['config'][0],
          'field_label' => $definition->getLabel(),
          'machine_name' => $definition->getName(),
          'field_group' => '-',
          'field_type' => $definition->getType(),
          'target_bundle' => empty($definition->referencedEntities()) ? '-' : 'array',
          'required' => $definition->isRequired() ? 'Yes' : 'No',
          'cardinality' => $field_storage_definition[$definition->getName()]->getCardinality(),
          'form_widget' => '-',
          'help_text' => '-',
        ];
      }
    }

    return $table_rows;
  }

}
