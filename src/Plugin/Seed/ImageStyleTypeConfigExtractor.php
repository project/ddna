<?php

namespace Drupal\ddna\Plugin\Seed;

use Drupal\ddna\Annotation\Seed;
use Drupal\ddna\SeedPluginBase;

/**
 * Plugin implementation of the seed.
 *
 * @Seed(
 *   id = "image_style_type_config_extractor",
 *   label = @Translation("Images Styles Type Config Extractor"),
 *   description = @Translation("Images Styles Type Config Extractor.")
 * )
 */
class ImageStyleTypeConfigExtractor extends SeedPluginBase {

}
