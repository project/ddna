<?php

namespace Drupal\ddna\Plugin\Seed;

use Drupal\ddna\Annotation\Seed;
use Drupal\ddna\SeedPluginBase;
use Drupal\migrate_plus\Entity\Migration;

/**
 * Plugin implementation of the seed.
 *
 * @Seed(
 *   id = "migrate_config_extractor",
 *   label = @Translation("Migrate Config Extractor"),
 *   description = @Translation("M`igrate Config Extractor.")
 * )
 */
class MigrateConfigExtractor extends SeedPluginBase {

  /**
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \JsonException
   */
  public function getElements(): string {
    return json_encode([
      'headers' => (object) $this->getTableHeader(),
      'rows' => $this->getTableRows(),
    ], JSON_THROW_ON_ERROR);
  }

  /**
   * @return string[]
   */
  protected function getTableHeader(): array {
    return [
      'migrate_label' => 'Migrate label',
      'migrate_machine_name' => 'Migrate machine name',
      'migrate_source_plugin' => 'Migrate source plugin',
      'migrate_destination_plugin' => 'Migrate destination plugin',
    ];
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTableRows(): array {
    $table_rows = [];
    $configs = \Drupal::service('ddna_config_matcher')
      ->matchAllConfigs($this->configuration['params']['regexp']);
    if (empty($configs)) {
      return [];
    }

    foreach ($configs as $element) {
      [, , $migration_id] = explode('.', $element);
      $migration = Migration::load($migration_id)->toArray();
      $table_rows[] = (object) [
        'migrate_label' => $migration['label'],
        'migrate_machine_name' => $migration['id'],
        'migrate_source_plugin' => $migration['source']['plugin'],
        'migrate_destination_plugin' => $migration['destination']['plugin'],
      ];
    }

    return $table_rows;
  }

}
