<?php

namespace Drupal\ddna\Plugin\Node;

use Drupal\ddna\Annotation\Node;
use Drupal\ddna\NodePluginBase;

/**
 * Plugin implementation of the node.
 *
 * @Node(
 *   id = "core",
 *   label = @Translation("Core"),
 *   description = @Translation("Core description.")
 * )
 */
class Core extends NodePluginBase {

}
