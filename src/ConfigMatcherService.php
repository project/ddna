<?php

namespace Drupal\ddna;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\StorageInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manages plugins for configuration translation mappers.
 */
class ConfigMatcherService {

  use ContainerAwareTrait;

  /**
   * The config storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $configStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
    StorageInterface $storage
  ) {
    return new static(
      $container->get('config.storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    StorageInterface $storage
  ) {
    $this->configStorage = $storage;
  }

  public function matchAllConfigs(string $regexp) {
    $configs = [];
    foreach($this->getAllConfigs() as $config) {
      if (preg_match($regexp, $config)) {
        $configs[] = $config;
      }
    }

    return $configs;
  }

  private function getAllConfigs() {
    if (empty($this->config)) {
      $this->config = $this->configStorage->listAll();
    }

    return $this->config;
  }
}
